# coding=utf-8
import datetime


class TrackFormatter:
    def __init__(self):
        pass

    def format_simple_itunes(self, track_file):
        track = {
            'title': track_file[0] if len(track_file[0]) > 0 else '',
            'artist': track_file[1] if len(track_file[1]) > 0 else '',
            'composer': track_file[2] if len(track_file[2]) > 0 else '',
            'album': track_file[3] if len(track_file[3]) > 0 else '',
            'genre': track_file[9] if len(track_file[9]) > 0 else '',
            'size': track_file[10] if len(track_file[10]) > 0 else '',
            'time': track_file[11] if len(track_file[11]) > 0 else '0',
            'disc number': track_file[12] if len(track_file[12]) > 0 else '0',
            'disc count': track_file[13] if len(track_file[13]) > 0 else '0',
            'track number': track_file[14] if len(track_file[14]) > 0 else '0',
            'track count': track_file[15] if len(track_file[15]) > 0 else '0',
            'year': track_file[16] if len(track_file[16]) > 0 else '0'
        }
        return track

    def format_simple_google(self, track_file):
        track = {
            'title': track_file['title'] if 'title' in track_file else '',
            'artist': track_file['artist'] if 'artist' in track_file else '',
            'composer': track_file['composer'] if 'composer' in track_file else '',
            'album': track_file['album'] if 'album' in track_file else '',
            'genre': track_file['genre'] if 'genre' in track_file else '',
            'size': track_file['estimatedSize'] if 'estimatedSize' in track_file else '',
            'time': str(int(float(track_file['durationMillis']) / 1000)) if 'durationMillis' in track_file else '',
            'disc number': str(track_file['discNumber']) if 'discNumber' in track_file else '0',
            'disc count': str(track_file['totalDiscCount']) if 'totalDiscCount' in track_file else '0',
            'track number': str(track_file['trackNumber']) if 'trackNumber' in track_file else '0',
            'track count': str(track_file['totalTrackCount']) if 'totalTrackCount' in track_file else '0',
            'year': str(track_file['year']) if 'year' in track_file else '0'
        }
        return track

    def format_track_to_itunes(self, track):
        """Convert the track to the correct iTunes parameters.

        Parameters
        ----------
        track : Dict
            All information about the track.

        Returns
        -------
        Dict
            The track with iTunes parameters.
        """

        # Fill in the necessary fields
        track_output = {
            'title': track['title'] if 'title' in track else '',
            'artist': track['artist'] if 'artist' in track else '',
            'composer': track['composer'] if 'composer' in track else '',
            'album': track['album'] if 'album' in track else '',
            'grouping': '',
            'work': '',
            'movement number': '',
            'movement count': '',
            'movement name': '',
            'genre': track['genre'] if 'genre' in track else '',
            'size': track['estimatedSize'] if 'estimatedSize' in track else '',
            'time': str(int(float(track['durationMillis']) / 1000)),
            'disc number': str(track['discNumber']) if 'discNumber' in track else '',
            'disc count': str(track['totalDiscCount']) if 'totalDiscCount' in track else '',
            'track number': str(track['trackNumber']) if 'trackNumber' in track else '',
            'track count': str(track['totalTrackCount']) if track['totalTrackCount'] else '',
            'year': str(track['year']) if 'year' in track else '',
            'date modified': datetime.datetime.fromtimestamp(int(track['lastModifiedTimestamp']) / 1000000.0).strftime(
                '%d-%m-%Y %H:%M'),
            'date added': datetime.datetime.fromtimestamp(int(track['creationTimestamp']) / 1000000.0).strftime(
                '%d-%m-%Y %H:%M'),
            'bit rate': '',
            'sample rate': '',
            'volume adjustment': '',
            'kind': '',
            'equaliser': '',
            'comment': track['comment'] if 'comment' in track else '',
            'plays': str(track['playCount']) if 'playCount' in track else '',
            'last played': datetime.datetime.fromtimestamp(int(track['recentTimestamp']) / 1000000.0).strftime(
                '%d-%m-%Y %H:%M'),
            'skips': '',
            'last skipped': '',
            'my rating': str(track['rating']) if 'rating' in track else '',
            'location': ''
        }
        return track_output
