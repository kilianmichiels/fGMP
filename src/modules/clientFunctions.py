from gmusicapi import Mobileclient
import sys


class Account:
    """Account class to maintain a login with the Google Music service and specify some handy functions."""

    def __init__(self, account, password, verbose):
        """
        The account creation.
        :param account: The account email address of the user.
        :param password: The password, provided by Google to access Google Services.
        :param verbose: Give extra information when the program is running or not.
        """
        self._account = account
        self._password = password
        self._verbose = verbose

        self._logged_in, self._api = self.login()

        if not self._logged_in:
            print('Failed to log in. Please check your credentials.')
            sys.exit(1)

        if self._verbose:
            print('Account created.')

    def login(self):
        """
        Log in to Mobile Client on Google Music.
        :return: Bool, MobileClient: Whether we successfully logged in and the mobile client.
        """
        api = Mobileclient()
        logged_in = api.login(self._account, self._password, Mobileclient.FROM_MAC_ADDRESS)
        return logged_in, api

    def fetch_playlists(self):
        """
        Fetch the playlists from the account.
        :return:
        """
        google_playlist_names = []

        playlists = self._api.get_all_user_playlist_contents()
        for playlist in playlists:
            google_playlist_names.append(playlist['name'])
        return google_playlist_names, playlists

    def fetch_playlist(self, playlist):
        """
        Fetch a specific playlist from Google Music. The names have to match.
        :param playlist: The playlist that needs to be found.
        :return: If the playlist is found, return it. If not, return None.
        """
        counter = 0
        found = False
        google_playlist_names, playlists = self.fetch_playlists()
        while counter < len(google_playlist_names) and found is False:
            if google_playlist_names[counter] == playlist:
                found = True
            else:
                counter = counter + 1

        if not found:
            print('fetch_playlist - Playlist {} not found.'.format(playlist))
            print('fetch_playlist - Other available playlists:')
            for play in google_playlist_names:
                print('\t--> {}'.format(play))
            return None
        else:
            return playlists[counter]

    def fetch_all_tracks(self):
        """
        Fetch all the tracks in the Google Music account.
        :return: Return the tracks if any are found. Else, return None.
        """
        all_tracks = self._api.get_all_songs()
        if len(all_tracks) > 0:
            return all_tracks
        else:
            print('fetch_all_tracks - Failed to fetch all tracks.')
            return None

    def upload_tracks_to_playlist(self, playlist_id, track_ids):
        """
        Upload tracks to the Google Music platform.
        :param playlist_id: The playlist ID in which the songs needs to be added.
        :param track_ids: The track IDs of the songs that need to be added to the playlist.
        :return:
        """
        if playlist_id is None:
            print('Error in upload_tracks_to_playlist: playlist is None')
            exit(1)
        elif len(track_ids) == 0:
            print('Error in upload_tracks_to_playlist: No tracks to add')
            exit(1)
        else:
            self._api.add_songs_to_playlist(playlist_id, track_ids)
