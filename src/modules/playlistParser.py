# coding=utf-8
import sys
from . import clientFunctions
from . import playlistFormat


class PlaylistParser:
    """Parser for the playlists."""

    def __init__(self, account, password, verbose):
        # Create an account to use the client functions
        self._account = clientFunctions.Account(account, password, verbose)
        self._playlist_formatter = playlistFormat.PlaylistFormatter(verbose)

        self._verbose = verbose
        self._failed = []
        self._success = []

    def save_google_playlists_to_txt(self):
        """Fetch the actual playlists from Google Play and save them as .txt files."""

        # Parameters for the playlist files
        TXTs = []

        # Fetch my playlists with their tracks
        google_playlist_names, playlists = self._account.fetch_playlists()
        for playlist in playlists:
            # Prepare the files to write
            TXTs.append(playlist['name'] + '.txt')

        if len(TXTs) > 0:
            if self._verbose:
                print('Playlists fetched.')
        else:
            print('Playlists not fetched.')
            exit(1)

        # Fetch al the songs
        all_tracks = self._account.fetch_all_tracks()

        if len(all_tracks) > 0:
            if self._verbose:
                print('Songs fetched.')
        else:
            print('Songs not fetched.')
            exit(1)

        # Start putting the tracks to the playlists
        for i, playlist in enumerate(playlists):

            # Create the playlist with the tracks
            TXT = self._playlist_formatter.format_google_playlist(playlist, all_tracks, False)

            if self._verbose:
                print('Writing to file...')
            write_success = self._playlist_formatter.write_itunes_playlist_to_txt_file(TXTs[i], TXT, 'Playlists')

            if self._verbose:
                print('Done with playlist {}.\n'.format(playlist['name']))
            else:
                sys.stdout.write(i * '.' + '\r')
                sys.stdout.flush()

            if write_success:
                self._success.append(TXTs[i])
            else:
                self._failed.append(TXTs[i])

        print('')
        self.return_stats()

    def return_stats(self):
        """Return a short summary of the function."""
        if len(self._success) > 0:
            print('Following playlists succeeded:')
            for playlist in self._success:
                print('\t--> {}'.format(playlist))
            print('')
        else:
            print('No playlists succeeded.')

        if len(self._failed) > 0:
            print('Following playlists failed:')
            for playlist in self._failed:
                print('\t--> {}'.format(playlist))
            print('\nHINT: Change the name of the failed playlists and try again!')
        else:
            print('No playlists failed.')
