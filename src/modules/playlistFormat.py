# coding=utf-8
from .trackFormat import TrackFormatter
import os


class PlaylistFormatter:
    """Class to format the different playlists accordingly."""
    def __init__(self, verbose):
        self._verbose = verbose
        self._track_formatter = TrackFormatter()
        self._itunes_header = 'Name	Artist	Composer	Album	Grouping	Work	Movement Number	Movement Count	' \
                              'Movement Name	Genre	Size	Time	Disc Number	Disc Count	Track Number	' \
                              'Track Count	Year	Date Modified	Date Added	Bit Rate	Sample Rate	' \
                              'Volume Adjustment	Kind	Equaliser	Comments	Plays	Last Played	' \
                              'Skips	Last Skipped	My Rating	Location '

    def write_itunes_playlist_to_txt_file(self, name, playlist, directory):
        """
        Write the iTunes playlist to a txt file.
        :param name: The name of the iTunes playlist.
        :param playlist: The playlist dictionary that need to be saved.
        :param directory: The path to where the playlist is saved.
        :return: True if playlist is successfully saved. Else, False.
        """

        # Create subdirectory for the playlists
        if not os.path.exists(directory):
            os.makedirs(directory)

        try:
            # Open text file
            my_file = open(directory + '/' + name, 'w')

            # If file is open
            if self._verbose:
                print('Successfully opened file.')

            # Create header in file
            my_file.write(self._itunes_header + '\n')

            tracks = ''
            for track in playlist:
                track_input = (
                        track['title'] + '\t' +
                        track['artist'] + '\t' +
                        track['composer'] + '\t' +
                        track['album'] + '\t' +
                        track['grouping'] + '\t' +
                        track['work'] + '\t' +
                        track['movement number'] + '\t' +
                        track['movement count'] + '\t' +
                        track['movement name'] + '\t' +
                        track['genre'] + '\t' +
                        track['size'] + '\t' +
                        track['time'] + '\t' +
                        track['disc number'] + '\t' +
                        track['disc count'] + '\t' +
                        track['track number'] + '\t' +
                        track['track count'] + '\t' +
                        track['year'] + '\t' +
                        track['date modified'] + '\t' +
                        track['date added'] + '\t' +
                        track['bit rate'] + '\t' +
                        track['sample rate'] + '\t' +
                        track['volume adjustment'] + '\t' +
                        track['kind'] + '\t' +
                        track['equaliser'] + '\t' +
                        track['comment'] + '\t' +
                        track['plays'] + '\t' +
                        track['last played'] + '\t' +
                        track['skips'] + '\t' +
                        track['last skipped'] + '\t' +
                        track['my rating'] + '\t' +
                        track['location'] + '\n'
                )

                tracks += track_input

            my_file.write(tracks)
            return True

        except IOError as e:
            # If file could not be opened
            print("Error for playlist {}!".format(playlist))
            print(e)
            print('---------------------------------------')
            return False

    def format_itunes_playlist_to_array(self, playlist):
        """
        Create an array of an iTunes playlist.
        :param playlist: The playlist which has to become an array.
        :return:
        """
        tracks = []
        try:
            my_file = open(playlist, 'r')
            playlist = my_file.read()
            playlist = playlist.split("\r")
            print(len(playlist))
            for x in range(1, len(playlist) - 1):
                track_file = playlist[x].split('\t')
                track = self._track_formatter.format_simple_itunes(track_file)
                tracks.append(track)
        except IOError:
            print('Could not open file {}'.format(playlist))
        return tracks

    def format_google_playlist(self, playlist, all_tracks, to_array):
        # Prepare an empty array for the playlist
        google_playlist = []

        # Prepare TXT File
        text_file = []

        # Get all tracks in the playlist
        tracks_in_playlist = playlist['tracks']

        for track in tracks_in_playlist:
            if self._verbose:
                print('Currently looking for {}'.format(track['trackId']))

            # Look for the track
            found_track, counter = self.find_google_track_in_google(track, all_tracks)

            if found_track:
                if to_array:
                    # Add track to array
                    google_playlist.append(self._track_formatter.format_simple_google(all_tracks[counter]))
                else:
                    text_file.append(self._track_formatter.format_track_to_itunes(all_tracks[counter]))

        if self._verbose:
            print('All tracks scanned.')

        if to_array:
            return google_playlist
        else:
            return text_file

    def find_google_track_in_google(self, track, all_tracks):
        # Find the track in all tracks
        found_track = False
        counter = 0
        while counter < len(all_tracks) - 1 and found_track is False:
            if all_tracks[counter]['id'] == track['trackId']:
                found_track = True
            else:
                counter = counter + 1
        # Inform
        if self._verbose:
            if found_track:
                print('--> Found track with ID: {}'.format(all_tracks[counter]['id']))
                print('--> This song is: {}'.format(all_tracks[counter]['title']))
            else:
                print('--> Track not found.')

        return found_track, counter

    # DOUBLE CODE
    # TODO: Remove this function
    def compare_song(self, song1, song2):
        if (song1['title'] == song2['title'] and
                song1['artist'] == song2['artist'] and
                song1['album'] == song2['album']):
            return True
        else:
            return False

    def find_itunes_track_in_google(self, track, all_tracks):
        # Find the track in all tracks
        found_track = False
        counter = 0
        if self._verbose:
            print('Looking for {} in {} songs...'.format(track['title'], len(all_tracks)))
        while counter < len(all_tracks) - 1 and found_track is False:
            if self.compare_song(track, all_tracks[counter]):
                found_track = True
            else:
                counter = counter + 1

        # Inform
        if self._verbose:
            if found_track:
                print('--> Found track with ID: {}'.format(all_tracks[counter]['id']))
                print('--> This song is: {}'.format(all_tracks[counter]['title']))
                return all_tracks[counter]
            else:
                print('--> Track not found.')
                return None
