import sys
import argparse
from modules.playlistParser import PlaylistParser
from importlib import reload
import time

if __name__ == "__main__":
    reload(sys)

    # Create parser
    argument_parser = argparse.ArgumentParser(
        description='Short program with missing but useful functions for Google Play '
                    'Music.')

    # Required arguments
    required = argument_parser.add_argument_group('required arguments')
    required.add_argument('-a', '--account', help='Your email address for logging in to Google Play Music',
                          required=True)
    required.add_argument('-p', '--password', help='Your password, generated on the security page. This is NOT your '
                                                   'default google password.', required=True)
    required.add_argument('-f', '--function', help='The function you would like to execute.', choices=['fetch'],
                          required=True)

    # Optional arguments
    argument_parser.add_argument('-v', '--verbose', help='If you want to see what is happening.', action="store_true",
                                 required=False)

    # Read arguments
    args = vars(argument_parser.parse_args())

    t0 = time.time()

    if args['function'] == 'fetch':
        if args['verbose']:
            print('Verbosity turned ON.')

        if not args['verbose']:
            print('Verbosity turned OFF. For more information about what is going on, run with -v')

        parser = PlaylistParser(args['account'], args['password'], args['verbose'])
        parser.save_google_playlists_to_txt()

    print('Duration: {0:.2f} seconds.'.format(time.time() - t0))
